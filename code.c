#include <LiquidCrystal.h>
#include <Keypad.h>

//ເຊັດຄ່າສຳຫຼັບ LCD
const int rs = A0, en = A1, d4 = A2, d5 = A3, d6 = A4, d7 = A5;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

//ເຊັດຄ່າສຳຫຼັບ Keypad 4x4
const byte ROWS = 4; 
const byte COLS = 4; 
char keys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};
byte rowPins[ROWS] = {13, 12, 11, 10};
byte colPins[COLS] = {9, 8, 7, 6};

Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);

//ເຊັດຄ່າສຳຫຼັບ Buzzer
const int buzzer = 5;

//ເຊັດຄ່າສຳຫຼັບ LED RGB
const int LED_red = 2;
const int LED_blue = 3;
const int LED_green = 4;

int base = 0; //ຄ່າທີ່ຈະຍົກກຳລັງ
int exponent = 0; //ໂຕກຳລັງ
bool isExponent = false; //ເອົາໄວ້ສຳຫຼັບເຊັດໂຕເລັກທີ່ຈະຍົກກຳລັງ ແລະ ໂຕກຳລັງ
bool clearedMessage = false; //ເອົາໄວ້ເຄຼຍຂໍ້ຄວາມ Hello

void setup() {
  Serial.begin(9600);
  lcd.begin(16, 2);
  pinMode(buzzer, OUTPUT);
  pinMode(LED_red, OUTPUT);
  pinMode(LED_blue, OUTPUT);
  pinMode(LED_green, OUTPUT);
  lcd.setCursor(0, 0);
  lcd.print("Hello this is");
  lcd.setCursor(0, 1);
  lcd.print("Power calculator");
}

void loop() {
  char key = keypad.getKey(); //ເອົາຄ່າມາຈາກ Keypad
  
  if (key) {
    if (!clearedMessage && (key >= '0' && key <= '9')) {
      //ເຄຼຍຂໍ້ຄວາມເມື່ອກົດ Keypad
      lcd.clear();
      clearedMessage = true;
    }
    
    if (key != 'A' && key != 'B' && key != 'C' && key != '*' && key != '#') { //ກວດແຕ່ຄ່າທີ່ເປັນໂຕເລກ
      int key_val = key - '0'; //ປ່ຽນຄ່າຈາກ Keypada ເປັນໂຕເລກ
      
      if (isExponent) { //ເຊັດຄ່າໂຕກຳລັງ
        if(key) {
          tone(buzzer, 440, 100);
        }
        exponent = exponent * 10 + key_val;
        lcd.write(key);
        delay(1000);
        calculateAndDisplayResult();
      } else { //ເຊັດຄ່າທີ່ຈະຍົກກຳລັງ
        base = base * 10 + key_val; //ເອົາຄ່າຈາກ Keypad ມາເກັບຄ່າໄວ້ 
        lcd.write(key);
      }
    } else if (key == '*') { 
      clearDisplay();
    } else if (key == 'A') { // '^' ໂຕດຳເນີນການ
      isExponent = true;
      lcd.write('^');
    }
    
    tone(buzzer, 440, 100);
  }
}

void calculateAndDisplayResult() {
  lcd.setCursor(0, 1);
  lcd.print("=");
  unsigned long result = integerPower(base, exponent);
  lcd.print(result);
  
  playSuccessTone(); //ເອີ້ນໃຊ້ຟັງຊັນຫຼິ້ນສຽງ 
  showLED(); //ເອີ້ນໃຊ້ຟັງຊັນໄຟກະພິບ
  clearDisplay(); //ເອີ້ນໃຊ້ຟັງຊັນຣີເຊັດ LCD
}

unsigned long integerPower(int base, int exponent) { 
  unsigned long result = 1;
  for (int i = 0; i < exponent; ++i) { 
    result *= base; //ເອົາຕົວເລກມາຄູນກັນຕາມຕົວຍົກກຳລັງ 
  }
  return result;
}

void clearDisplay() { //ຟັງຊັນສຳຫຼັບຣິເຊັດ LCD ແລະ ຄ່າຕ່າງຽ 
  lcd.clear();
  base = 0;
  exponent = 0;
  isExponent = false;
  clearedMessage = false;
  lcd.setCursor(0, 0);
  lcd.print("Hello this is");
  lcd.setCursor(0, 1);
  lcd.print("Power calculator");
  analogWrite(LED_red, 0);
  analogWrite(LED_green, 0);
  analogWrite(LED_blue, 0);
}

void playSuccessTone() { //ສຽງຕອນຜົນລັບອອກມາ
  tone(buzzer, 1000, 200); 
  delay(200);              
  tone(buzzer, 1200, 200);
  delay(200);              
  tone(buzzer, 1000, 200); 
  delay(200); 
  noTone(buzzer); 
}

void showLED() { //ການກະພິບຂອງ LED RGB ໃນຕອນຜົນລັບອອກມາ
  analogWrite(LED_red, 100);
  analogWrite(LED_green, 100);
  analogWrite(LED_blue, 155);
  delay(1000);
  analogWrite(LED_red, 150);
  analogWrite(LED_green, 200);
  analogWrite(LED_blue, 195);
  delay(1000);
  analogWrite(LED_red, 105);
  analogWrite(LED_green, 110);
  analogWrite(LED_blue, 55);
  delay(1000);
}
